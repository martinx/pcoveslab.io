+++
title = "I was wrong: 36 is too much"
description = "How I went even further down and surprised myself"

[taxonomies]
about = ["Keyboard", "QMK", "36", "keys", "combo", "tap", "dance"]

[extra]
thumbnail = "qmk.png"
+++

I somewhat very recently posted about [How a 36 keys keyboard suits my needs as a developer][previous-article].

And thanks to a fellow reader [@Kaze][kaze], the brain behind the [*Lafayette*][lafayette] layout, I was able to reduce even further down without reducing my typing speed.
Even better, I was able to do so while getting rid of the [*tap dance*][tap-dance] feature.

In this post I'll show how I did such a thing by following the step-by-step trial and error I went through.
And guess what: if 36 is too much, so is 34.
Let's go down right to 32 keys!

<!-- more -->

---

{% quote(author="#SharpenTheSaw") %}
I'm typing this content on the newly modified *layout*.
As you'll see, I moved some "programming" characters around, namely `()[]{}<>`.
This website is redacted in [Markdown][markdown] which relies on some of those characters, most notably for links.

I'm not *fluent* yet.
I mistype some of these characters quite frequently.
But I've been through this during the previous iterations of my layout and I know it'll be over by the end of the week.
{% end %}

---

## What do I have?

{% figure(path="layout_0.png") %}
A visual representation of the initial layout
{% end %}

It may seem intimidating at first.
Let's break it down into understandable pieces.

* It's a regular [*QWERTY*][qwerty] layout for the *alpha* keys
* The center key of the right *thumb cluster* gives access to a *layer* if held
* The center key of the left *thumb cluster* acts as *Shift* if held
* The *layer* contains the *numbers* on the first row so using both thumbs at the same time gives access to the shifted numbers' characters
* The **blue** keys, if tapped send a *keycode* and an `Alt` modified *keycode* if held. This is because I use [`i3`][i3] as my window manager and it helps me move between workspaces using a single-held key.
* The **orange** keys are *tap-mod* keys: they act as a key if tapped and as a modifier (Shift for the lighter ones, Control for the darker ones) if held.
* The **green** keys are *tap-dance* keys: a single key sends different *keycodes* depending if it's tapped, held, double-taped, tap-held, double-tap-held, etc.
* The **pink** keys are *combo* keys: taping `S` will send `S` and tapping `F` will send `F` but tapping `SF` will send `Tab`.

That's all.

## Why go further?

I said a word about this at the end of my [previous article][previous-article]: my left thumb is over-solicited.

For a long time, I managed [`i3`][i3] with the `Alt` key, not the `Gui` one.
Meaning my left thumb was in charge of:
* The window manager
* `Shift` and `Enter`
* `([{<`

That's a lot for a single key.
Even for a strong finger such as the thumb.

The **blue** keys are a very recent addition to this layout and so I spent the last two years using `Alt + qwertyuiop` to switch workspaces.
It implies bending the left thumb under the palm while reaching for a top-row letter.

And it's painful now.

## How to improve this?

At first, I thought balancing the use of my thumbs would be enough.
But the more I dug into this, the more it appeared that there were better alternatives.

### `Alt` and `Gui`

My first move was to go from `Alt` to `Gui` for [`i3`][i3] and swapped their positions.

I'd still have to use the left thumb to manage windows and workspaces.
But I'd have to resort to the right one for the `Alt` *tap-dance*.

This might seem like a small improvement.
But as I write in French, I use `Alt Gr` quite a lot for [diacritics][diacritics] such as `Ç` and `Œ`.
So, not that small after all as I started using my right thumb.

{% figure(path="layout_1.png") %}
A visual representation of the first layout
{% end %}

### The inner thumb keys

By *inner* keys I mean the right-most key of the left half and the left-most key of the right half.
The two *tap-dance* keys containing `([{<` and `)]}>` respectively.

Thanks to [@kaze][kaze], I moved them to the *layer* accessible thanks to the right thumb.
This not only frees two thumb keys but also lightens the burden on the left thumb a lot as I added them to the right-hand side.

* `[` goes on the first *layer* on the `N` key accessible through the right thumb. And `{` is its shifted version thanks to the left thumb.
* `>` goes on the first *layer* on the `M` key accessible through the right thumb. And `}` is its shifted version thanks to the left thumb.
* `(` goes on the first *layer* on the `,` key accessible through the right thumb.
* `)` goes on the first *layer* on the `.` key accessible through the right thumb.
* Both `<` and `>` are simply the default shifted versions of `,` and `.`. No magic here.

{% figure(path="layout_2.png") %}
A visual representation of the second layout
{% end %}

34 keys! Awesome!

### The outer thumb keys

I thought this was enough.
But I then gave the following a try and I'm really happy with it.

#### `Alt`, `Alt Gr`, and `Shift Alt Gr`

I previously had my `Alt`, `Alt Gr`, and `Shift Alt Gr` *tap-dance* keys next to the `Shift` under the left thumb.

Now, it's under the right thumb.
As such, I can freely `Shift` with the left one while `Alt Gr` with the right one.
So, `Shift Alt Gr` is not needed anymore.

But why keep the `Alt` and `Alt Gr` *tap-dance* at all?
And why keep this key under the thumb while I have **plenty** of room available on the *alpha* rows for free.
Why not move both `Alt` and `Alt Gr` up a bit?

Just like a tapped `A` sends `A` and a held one acts as `Control`, I can do it with both `Alt` and `Alt Gr`.
The former goes to `H` and the later to `G`.
This way, I can still easily access `Shift Alt Gr` with the left hand and the [diacritics][diacritics] are built using my right hand.

{% figure(path="layout_3.png") %}
A visual representation of the third layout
{% end %}

#### `Gui` or whatever its name is

A little digression here: **WTF** is this name?
Why would someone name a key `GUI`?
It already has a meaning, go invent something else!

Anyway.
I still have quite a great amount of free space on the *alpha* keys.
I should enjoy it more.

Same player plays again: `X` and `.` act as normal keys if tappped and as `Gui` if held.

{% figure(path="layout_4.png") %}
A visual representation of the fourth layout
{% end %}

32 keys! Terrific!

## Conclusion

I complained about thumb pain to fellow keyboards and custom layout enthusiasts.
Thanks to his insight, it took only a couple of hours to go from *less thumb usage* to *almost no thumb usage*.

Now, I want to ditch my *GergoPlex* in favor of a 32 keys keyboard!
Just kidding, I have got a nifty little hack to share in my next article that makes use of one of those deleted keys!

If you like my write-ups, don't hesitate to share them with your friends.
And if you dislike them, don't hesitate to share them with your foes.

Also, you can come and tell me your thoughts about it on [Mastondon][mastodon]!

[previous-article]: /blog/keyboard-36-keys/
[kaze]: https://mamot.fr/@fabi1cazenave@mastodon.social
[lafayette]: https://qwerty-lafayette.org/
[tap-dance]: https://docs.qmk.fm/#/feature_tap_dance
[markdown]: https://daringfireball.net/projects/markdown/
[qwerty]: https://en.wikipedia.org/wiki/QWERTY
[i3]: https://i3wm.org/
[diacritics]: https://en.wikipedia.org/wiki/Diacritic
[mastodon]: https://mamot.fr/@PacoVelobs
