+++
title = "Workflow update"
description = """
Sharpen the saw: identify painpoints, find a set of solution, pick the best and turn it into an habit.
It works for `vim` but also on a bigger scale such as a global workflow.
I think I updated mine since the beginning of this site and it's time to write about it.
"""

[taxonomies]
about = ["workflow", "vim", "nvim", "tmux", "alacritty"]
+++

I somewhat recently got a new job.
With that a new machine, brand new OS ready for customization.
Along a pile of new attributed tasks and its load of meetings of course.

In this post, I write about what I changed in my workflow to better accommodate this changes.
I'll talk about minor `nvim` changes, `alacritty` _hacks_ and a newly-discovered `tmux` feature and a quick feed back on my new favorite synchronization tool `syncthing`.

Let's dive right in!

<!-- more -->

## The tool

A long time ago, I used [`vimwiki`][vimwiki] to take notes.
It's a fine tool which supports both the `wiki` syntax and markdown `markdown` syntax.
I picked the later, mainly because its well supported across many other tools I use daily.
What's great about [`vimwiki`][vimwiki] is it's ability to jump from files to files just like we're used to with hyperlinks on the web.
What's not that great is the limitations of both the supported syntaxes.

Enter [`neorg`][neorg], a `nvim` plugin built on top of the `norg` syntax.
It brings many features into the note-taking game which deserve more than a paragraph in a blog post.
If I've to pick one as an example, the ability to lower the *level* of text with `---` which decrements by one and `===` which resets to 0.
It helps me write better structured and more detailed notes.
I find `norg` to be very promising even if [`neorg`][neorg] is still a work in progress.

## The hack

I'm a convinced single-screen user.
In find multiple-screens setup to induce neck pain so, here I am, rocking a tiling window-manager and switching workspaces and attached applications in a blink while remaining still.

But, sometimes, one screen falls short.
Notably, during meetings: I want full-screen presentation **and** note-taking.
I figured I could have the best of both worlds thanks to `alacritty`'s transparency.
Here is a small set of aliases I've in my *shell* configuration:

```bash
function tt {
    yq -y -i ".window.opacity = $1" ~/.config/alacritty/alacritty.yml
}

function ttt {
    opacity=$(yq .window.opacity ~/.config/alacritty/alacritty.yml)
    opacity=$(awk '$0<.7 ? $0=.95 : $0=.5 1' <<< $opacity)
    tt $opacity
}

function t~ {
    opacity=$(yq .window.opacity ~/.config/alacritty/alacritty.yml)
    opacity=$(( $opacity + $1))
    opacity=$(awk '($0<0){$0=0} ($0>1){$0=1} 1' <<< $opacity)
    tt $opacity
}

alias t+="t~ +.05"
alias t-="t~ -.05"
```

What's going on here?
1. The function `tt` takes an opacity level as it first and only input. It uses it to update `alacritty`'s setting.
2. The function `ttt` circle between two predefined settings: `.5` and `.95`.
3. The function `t~` is not meant to be used by hand but it comes in handy for the `t+` and `t-` aliases.
   It's goal is to increase/decrease the opacity while staying between the authorized `[0;1]` limits.

Enough code, what is it useful for?
Well, when I need to take notes during a meeting, I put the terminal on top of the presentation, both in full-screen mode and type `ttt`.
I can now take notes while watching the speaker and its slides through `alacritty`.
Once done, `ttt` to get back to my default opacity level and exit full-screen.

Simple, fast and efficient, also, works in desktop and laptop mode.
What's not to like?

## The bulky

I've said it in other blog posts, I heavily rely on `tmux` in my workflow.
I manage one `tmux` session per project.
Each `tmux` session generally contains a `nvim` session which gets restored by `tmux` which gets opened by `alacritty` which gets launched automatically when I open my user session.
Sounds like a dream come true and it kind of is.

**But**, when I'm in _project X_ and want to access my notes which happened to also be opened in _project Y_, `nvim` yells at me.
Something along me opening files/buffers that are already accessed somewhere else at the same time.
And I don't like to be yelled at.
I could find a way to disable this check (specifically for `norg` files?) while enabling auto-reloading buffers when the underlying file gets updated.
But I went down another path: I have a notes dedicated `tmux` session.

At first it was rather annoying to switch from wherever I was working to my notes and back.
But I finally found a pretty handy set of shortcuts:
1. I _mark_ the note-taking `tmux` session's single window with `Prefix-m`.
   Note the capital `M` right next to the window's name.
2. I bind `Prefix-N` (for `norg`) as follow: `bind N switch-client -t '{marked}'`.
   It allows me to jump to my notes from any session/window/pane.
3. I installed `tmux`'s `sessionist` plugin `set -g @plugin "tmux-plugins/tmux-sessionist"`.
   It gives more sensible keybindings, notably `Prefix-S` to circle between the last two sessions.

Basically, I work, press `Prefix-N`, input notes, press `Prefix-S` and carry on.

## Conclusion

I needed to be efficient during the onboarding process for my new job.
I had to be able to jump from one task to another while joining meetings with important informations for later.
I'd been in the same company for six years before and had _everything_ somewhere in my head after this time.
But starting the memorization process all over again was too much without a second brain by my side.

This set of commands/tools/settings made it possible.
With a bit of _zettlekasten_ (I'll let you search this one on your own for now) it all came together pretty well.
Last but not least, I already wrote about it so I'll just add the link [here](/en/blog/syncthing/).
I ditched `nextcloud` in favor of `syncthing`: it has a _do more, cost less_ vibe I enjoy as I don't pay for my `nextcloud` server, thus reducing my costs while having my more data redundancy.

I never brought me any feedback before but I'll do it anyway.
Who knows, maybe it'll work this time.
I this post brought you joy, share it with your friends and don't hesitate to ping [me](https://mamot.fr/@PacoVelobs) on the _fediverse_.
If it pained you to read me rambling about my workflow, share it to your friends too and have a mock me together!

[vimwiki]: https://github.com/vimwiki/vimwiki
[neorg]: https://github.com/nvim-neorg/neorg
