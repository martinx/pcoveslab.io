+++
title = "Blog"
description = "Comment retrouver ce dont j'ai trop souvent besoin mais pas assez fréquemment pour m'en souvenir."

weight = 0

sort_by = "date"
paginate_by = 6

generate_feed = true
+++

C'est ici que je divague.
Toujours à propos de technologie mais rarement la même d'un article à l'autre.

Que ce soit sur la programmation, les outils de développement ou des défis techniques en pro ou en perso, j'essaie de décrire le problème, d'énumérer les solutions envisagée et d'expliquer celle choisie.

Je partage aussi des *tutos* que je fais à destination de mes collègues quand une même question revient souvent.

Bonne lecture!
