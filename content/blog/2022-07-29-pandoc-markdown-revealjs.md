+++
title = "Présentations RevealJS en Markdown avec Pandoc"
description = "Fonctionnalité de pandoc pour réaliser des présentations revealjs en markdown"

draft = false

[taxonomies]
sujets = ["IT", "revealjs", "pandoc", "markdown"]

[extra]
thumbnail = "revealjs.jpg"
+++

Il m'arrive fréquemment de faire des présentations au reste de mon équipe ou même à l'entreprise si l'occasion se présente.
C'est un exercice que certains vivent comme une corvée; Je vois cela comme une façon de conclure un projet: si je ne peux pas le présenter simplement, c'est que le travail n'est pas terminé.

Très différents de la documentation technique, les *slides* présentent une vision haut niveau des problèmes abordés et doivent être clairs, concis et agréables à l'œil.
Un peu comme un *changelog* donne la vision haut niveau pour le grand public et les *commits* servent le détail aux développeurs spécialisés.

Avant (était-ce mieux?) je rédigeais ces documents en `LaTeX` avec `Beamer`.
C'est verbeux, parfois tatillon mais le résultat est généralement de qualité pour ce qui est de la mise en forme finale.

Un collègue m'a introduit à [`RevealJS`][revealjs] et surtout la possibilité de rédiger le contenu en `Markdown` qui est tout de même moins verbeux que `LaTeX` en conservant une grande partie de l'expressivité de ce dernier.
Cet article n'est pas un tuto mais plus une liste des fonctionnalités que j'utilise généralement dans ces présentations.

<!-- more -->

# L'en-tête

`Pandoc` est un programme permettant de convertir des fichiers parmi une myriade de formats.
Ici, c'est le passage de `Markdown` à `HTML` qui nous intéresse.

Un document `Markdown` dans ce contexte commence par un en-tête définissant certaines variables.
J'utilise à minima le code suivant:

```yaml
title: Title
subtitle: Subtitle
date: DD/MM/YYYY
author: PCoves
```

Cela sert essentiellement à générer la première page, l'équivalent du `\titlepage` de `LaTeX/Beamer`.

# `Pandoc`

J'aborde ici les différents paramètres que je passe à `pandoc` lors de la conversion.
Certains activent ou non des fonctionnalités, d'autres relèvent plus de la préférence personnelle.

## Formats

Au début, j'utilisais simplement l'entrée `-f/--format markdown` en passant mon fichier source `Markdown` à convertir.

J'ai par la suite découvert `-f/--from markdown+emoji`.
Cet ajout permet d'utiliser des *emojis* dans les *slides* sans avoir à s'embêter avec de l'Unicode ou des images externes :ok_hand:

Pour la sortie, un simple `-o/--output output.html` suffit.

## Type de document

Il n'est pas possible pour `pandoc` de découvrir automatiquement que je cherche à générer une présentation [`RevealJS`][revealjs].
Le paramètre `-t/--to revealjs` est donc à renseigner obligatoirement.

Il faut aussi indiquer **quelle version** de [`RevealJS`][revealjs] utiliser et **où** la trouver.
Pour ma part, j'essaie de toujours utiliser la dernière version et je ne veux pas m'encombrer à aller la chercher à chaque fois.
Par chance, il est possible de renseigner une URI et le CDN `unpkg` donne accès à la version la plus récente comme suit: `-V revealjs-url=https://unpkg.com/reveal.js@latest/`.

## Les différents niveaux

Pour retrouver les `(sub)*section` et `(sub)*paragraph` du `LaTeX`, j'utilise le paramètre `--slide-level=3`.

Concrètement, cela signifie que mes titres de niveau `1 (#)`, `2 (##)` et `3 (###)` en `Markdown` créeront de nouvelles pages et je les vois comme des `section`, `subsection` et `subsubsection` de `LaTeX`.
Les titres de niveaux `4 (####)`, `5 (#####)` et `6 (######)` iront à la suite de la page courante et que les vois comme les `paragraph`, `subparagraph` et `subsubparagraph` de `LaTeX`.

Pour forcer un saut de page entre les *paragraphes*, il suffit d'ajouter une ligne vide puis une ligne contenant trois tirets puis une ligne vide.

## Mise en forme

### Les thèmes

Il existe un choix satisfaisant de [thèmes](https://revealjs.com/themes/) pour les présentations.
La sélection est très simple avec le paramètre `-V theme=nom_du_theme`.

### Numérotation

Par défaut, les numéros de parties/pages ne sont pas affichés.
Un simple `-V snodeNimber=true` corrige ce comportement.

## Schémas

J'adore les schémas!
Et comme une image vaut mille mots, je ne me prive pas de les utiliser dans mes présentations.

J'intègre l'excellente bibliothèque [`PlantUML`](https://plantuml.com/) grâce à `-F/--filter pandoc-plantuml`.
Cependant, elle demande une ou deux dépendances selon les schémas à générer.
Afin de ne pas encombrer inutilement mon système de manière globale, je passe par un environnement virtuel `python`:

```bash
python3 -m venv venv
. ./venv/bin/activate
pip install pandoc-plantuml-filter
```

## *Debug/Release*

Ok, *debug* et *release* sont de bien grands mots dans le contexte d'une présentation.
Mais il y a deux derniers paramètres à connaître.

### *Standalone*

Le paramètre `-s/--standalone` est à renseigner à minima.
Il permet de générer un fichier `HTML` complet en sortie au lieu de juste le contenu du *tag* `<body />`.

Avec ce paramètre, les images et bibliothèques seront chargées à l'ouverture du fichier de sortie.
Cela implique une génération rapide et une sortie légère mais internet sera nécessaire lors de la présentation.

### *Self contained*

Le paramètre `--self-contained` recouvre implicitement `-s/--standalone`.

Le fichier en sortie sera plus lourd et sa génération plus lente.
Mais les images sont embarquées en *base 64* et les bibliothèques *JS* sont copiées dans le code du document.
De fait, la présentation peut avoir lieu sans aucune connexion et il est très facile de la partager à autrui.

# Syntaxe

Il y a assez peu de choses à savoir en plus du `Markdown` initial.

`Pandoc` utilise des *fenced blocks* pour indiquer un contexte spécifique à une zone du code.
Un *fenced block* s'ouvre avec au minimum trois deux-points suivi du nom du contexte et se ferme avec le même nombre de deux points.
Cette syntaxe permet d'imbriquer les *fenced blocks* en augmentant le nombre de deux-points.

## Colonnes

Pour générer une *slide* avec plusieurs colonnes, rien de plus simple:

```markdown
::: columns
:::: column
Contenu de gauche
::::
:::: column
Contenu de droite
::::
:::
```

Il faut parfois adapter la largeur des colonnes si un côté a besoin de notablement plus de place que l'autre.
C'est bien évidemment possible:

```markdown
::: columns
:::: {.column width="75%"}
Contenu de gauche large
::::
:::: {.column width="25%"}
Contenu de droite étroit
::::
:::
```

## Listes

Les listes en `Markdown` sont des lignes débutant par `*` ou `N.` avec `N` un numéro.
Il est possible de les faire apparaître les unes à la suite des autres comme suit:

```markdown
::: incremental
* Foo
* Bar
* Baz
:::
```

## Notes de lecture

Les *speaker notes* sont des morceaux de la présentation, invisibles à l'auditoire mais bien montrés au présentateur dans une autre fenêtre:

```markdown
::: notes
Ceci ne sera pas visible sur la présentation principale mais accessible à la personne qui parle
:::
```

Pour faire apparaître cette fameuse fenêtre il suffit d'appuyer sur la touche `s` depuis la présentation dans le navigateur web.

# Résumé

```bash
python3 -m venv venv
. ./venv/bin/activate
pip install pandoc-plantuml-filter

pandoc -t revealjs -V revealjs-url=https://unpkg.com/reveal.js@latest/ --slide-level=3 -V theme=solarized -V slideNumber=true -F pandoc-plantuml -f markdown+emoji -o index.html index.md --standalone      # Debug
pandoc -t revealjs -V revealjs-url=https://unpkg.com/reveal.js@latest/ --slide-level=3 -V theme=solarized -V slideNumber=true -F pandoc-plantuml -f markdown+emoji -o index.html index.md --self-contained  # Release
```

# Notes

* Appuyer sur `f` passe en mode *full-screen*.
* Appuyer sur `o` passe en mode *overview*.

[revealjs]: https://revealjs.com/
