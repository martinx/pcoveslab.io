+++
title = "Lignes multiples en YAML"
description = "Pense-bête sur la syntaxe YAML"

[taxonomies]
sujets = ["Code", "yaml", "ansible", "alacritty", "config"]

[extra]
thumbnail = "yaml.svg"
+++

Ces derniers temps, j'ai beaucoup travaillé sur les *pipelines* d'intégration continue de `gitlab`.
La configuration de ces derniers ce fait en `yaml` et il n'est pas rare d'avoir de très longues commandes.
Pour des raisons de lisibilité évidentes, il peut être pratique de les afficher sur plusieurs lignes.

Seulement voilà, `yaml` a plusieurs approches pour *splitter* les lignes.
Et vu que je ne me souviens jamais vraiment de qu'est-ce qui fait quoi et que j'en ai marre de farfouiner au fond des internets: un nouveau pense-bête!

<!-- more -->

Tout d'abord, il faut rendre à César ce qui est à César.
La bible sur laquelle je retourne toujours se trouve sur [StackOverflow][source] et va bien plus dans les détails que moi.
Je ne garde ici que ce qui me sert réellement dans mon quotidien de dévelopeur.

### Supprimer les sauts de lignes

Il s'agit de la syntaxe dite [*folded*](https://yaml.org/spec/1.2-old/spec.html#style/block/folded) dans la spécification `yaml`.

La syntaxe la plus simple quand on veut juste mettre **une** commande sur **plusieurs** lignes utilise le caractère `>`.
Les deux blocs suivants sont parfaitement équivalents:

```yaml
key: >
    value0
    value1
    value2
```

```yaml
key: value0 value1 value2
```

À noter que dans les deux cas, la chaîne de caractères qui en résulte contient un saut de ligne final: `"value0 value1 value2\n".

### Conserver les sauts de lignes

Il s'agit de la syntaxe dite [*literal*](https://yaml.org/spec/1.2/#style/block/literal) dans la spécification `yaml`.

Pour avoir plusieurs lignes en une seule entrée, pratique lorsque l'on veut garder une mise en forme, c'est `|` qu'il faut utiliser.

```yaml
key: |
    value0
    value1
    value2
```

La valeur dans le bloc précédent, bien que associé à une seule clé, contient des sauts de lignes: `value0\nvalue1\nvalue2\n`.
À noter que là encore, il y a un saut de ligne final.

### Contrôler la fin des blocs

Il s'agit de la syntaxe dite [*chomping*](https://yaml.org/spec/1.2/#chomping/) dans la spécification `yaml`.

Dans les deux parties précédentes, un saut de ligne `\n` final était systématiquement présent.
Il est possible de le supprimer ou au contraire d'en rajouter.

#### Supprimer le saut de ligne final

Il faut pour cela ajouter un `-` au `>` ou `|`.

```yaml
key: >-
    value0
    value1
    value2
```

```yaml
key: |-
    value0
    value1
    value2
```

Les deux blocs précédents sont alors respectivement équivalents a `"value0 value1 value2"` et `value0\nvalue1\nvalue2`.

#### Ajouter des espaces en fin de bloc

L'usage d'un `+` à la place d'un `-` va au contraire préserver les sauts de lignes supplémentaires si présents.
Je ne vois pas dans quel contexte cela peut être utile avec `>` mais je comprends tout à fait l'usage avec `|`.

```yaml
key: |+
    value0
    value1
    value2



```

Le bloc précédent avec les trois lignes vides sera équivalent a `value0\nvalue1\nvalue2\n\n\n`.

### Bonus

Dans les commentaires de ma [source][source], quelqu'un a publié un moyen mnémotechnique pour se souvenir plus facilement de la différence entre `>` et `|`.
Il voit ces deux caractères comme des filtres indiquant à gauche la taille du contenu d'entrée en nombre de ligne et à droite la taille de sortie.
Autrement dit, il voit `>` comme un "compresseur" car plus large avant qu'après et `|`, à l'inverse comme un "neutre", aussi étendu avant qu'après.

[source]: https://stackoverflow.com/a/21699210
