+++
title = "Création de clés USB chiffrées"
description = "Comment créer et utiliser un support chiffré sur Linux"

[taxonomies]
sujets = ["IT", "CLI", "Clé USB", "Chiffrement", "Sécurité", "Cryptsetup", "LUKS"]

[extra]
thumbnail = "luks.jpg"
+++

Il y a fort longtemps (non) dans une vie lointaine (non) j'ai rédigé un tutoriel pour mes collègues (oui) afin que tous sachent comment chiffrer des supports de stockages et que les données sensibles de l'entreprise ne se baladent plus à la merci d'une clé USB perdue.

Ce texte était au départ dans un wiki interne.
Mais vu qu'il a été rédigé sur mon temps libre, je le récupère pour le mettre ici.

<!-- more -->

# Création de clé USB chiffrée

Le noyau **Linux** propose le *module* `dm_crypt` (*device mapper cryptographic*).
Il s'agit d'un outil intégré au noyau en charge de (dé)chiffrer à la volée des *volumes* tels que des disques durs ou des clés USB.

Cependant, même si une partition chiffrée comme expliqué dans ce guide n'est lisible que sur *Linux*, il est possible de faire cohabiter des partitions chiffrées et non chiffrées sur un même support physique.
De fait, ce guide doit être suivi sur *Linux* mais concerne les usagers de tous les systèmes d'exploitations.

## Création des partitions

Un support de stockage peut contenir une seule ou plusieurs partitions séparées.
Il existe de nombreux outils en console ou avec interface graphique dédiés à la création et la manipulation des partitions : `fdisk` (CLI) ou `parted` (CLI) et `gparted` (GUI).

Soit une clé USB présente sur `/dev/sda`.
```
❯ lsblk
NAME        MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda           8:0    1   3.8G  0 disk
[...]
```

La suite de ce guide montre comment créer deux partitions et en chiffrer une.
```
❯ sudo fdisk /dev/sda

Welcome to fdisk (util-linux 2.36.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): g
Created a new GPT disklabel (GUID: 0980954A-46E8-A548-81B3-A40C51D919E6).

Command (m for help): n
Partition number (1-128, default 1):
First sector (2048-7866334, default 2048):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-7866334, default 7866334): +2G

Created a new partition 1 of type 'Linux filesystem' and of size 2 GiB.

Command (m for help): n
Partition number (2-128, default 2):
First sector (4196352-7866334, default 4196352):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (4196352-7866334, default 7866334):

Created a new partition 2 of type 'Linux filesystem' and of size 1.7 GiB.

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
```

Les étapes sont les suivantes :
1. `g` permet de créer une nouvelle table de partition au format `GPT`.
2. `n` permet de créer une nouvelle partition.
    1. Le numéro de partition est laissé par défaut (ici `1`).
    2. Le premier secteur (là où commence la partition) est laissé par défaut.
    3. Le dernier secteur (là ou termine la partition) est mis à `+2G`, soit une partition de `2Go`.
3. `n` permet de créer une nouvelle partition.
    1. Le numéro de partition est laissé par défaut (ici `2`).
    2. Le premier secteur (là où commence la partition) est laissé par défaut.
    3. Le dernier secteur (là où termine la partition) est laissé par défaut pour utiliser tout l'espace restant.
4. `w` permet d'écrire ces modifications sur la clé.

On retrouve bien les deux partitions dont la première de `2Go` et la seconde sur tout l'espace restant soit `1.7Go`.
```
❯ lsblk
NAME        MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda           8:0    1   3.8G  0 disk
├─sda1        8:1    1     2G  0 part
└─sda2        8:2    1   1.7G  0 part
[...]
```

## Chiffrement d'une ou plusieurs partitions

Soit une clé USB présente sur `/dev/sda` et dotée de deux partitions `sda1` et `sda2`.
La suite de ce guide montre comment chiffrer et utiliser `sda1` tout en gardant `sda2` non chiffrée.

```
❯ sudo cryptsetup luksFormat /dev/sda1

WARNING!
========
This will overwrite data on /dev/sda1 irrevocably.

Are you sure? (Type 'yes' in capital letters): YES
Enter passphrase for /dev/sda1:
Verify passphrase:
```

Ici, la commande `cryptsetup` est appelée avec la sous-commande `luksFormat` pour initialiser une partition *Linux Unified Key Setup*.
Lors de l'appel de cette commande, une *phrase de passe* est demandée.

Il faut maintenant demander au système d'ouvrir ce volume chiffré.
```
❯ sudo cryptsetup open /dev/sda1 usb
Enter passphrase for /dev/sda1:
```

Lors de l'ouverture du volume chiffré, une *phrase de passe* est demandée.
Sans surprise, c'est la *phrase de passe* renseignée lors de l'initialisation de la partition *LUKS*.

```
❯ lsblk
NAME        MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda           8:0    1   3.8G  0 disk
├─sda1        8:1    1     2G  0 part
│ └─usb     254:2    0     2G  0 crypt
└─sda2        8:2    1   1.7G  0 part
[...]
```

## Création d'un système de fichiers

Pour le moment, `/dev/sda` a deux partitions:
* `sda1` chiffrée.
* `sda2` non chiffrée.

Cependant aucune ne contient de système de fichiers et donc aucune n'est actuellement utilisable.

La partition chiffrée n'étant utilisable que sur *Linux*, il est raisonnable d'y mettre un système de fichiers propre à ce système: `ext4` fera très bien l'affaire.
```
❯ sudo mkfs.ext4 /dev/mapper/usb
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 520192 4k blocks and 130048 inodes
Filesystem UUID: f44e1f20-afdc-4022-80a3-69178aeaccaf
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

La partition non chiffrée quant à elle doit pouvoir être utilisée sur tous les systèmes d'exploitation, un système de fichiers `fat32` sera approprié.
```
❯ sudo mkfs.fat -F32 /dev/sda2
mkfs.fat 4.1 (2017-01-24)
```

> Attention : il faut passer par `/dev/mapper/usb` pour formater la partition chiffrée à travers `dm_crypt` alors qu'il suffit d'accéder directement a `/dev/sda2` pour accéder à la partition non chiffrée.

```
> sudo cryptsetup close usb
```

Voilà, la clé est prête à être retirée et utilisée sur d'autre machines.

## Usage

### Montage

La plupart des systèmes d'exploitation modernes sauront monter automatiquement la partition non chiffrée.
Cependant, dans un souci d'exhaustivité, toutes les étapes seront traitées pour les deux partitions.

Après branchement de la clé USB sur une machine, il faut déchiffrer la partition chiffrée puis monter les deux partitions.
Il est bien sûr possible de ne monter que la première après déchiffrement ou que la seconde sans s'occuper de la première.

```
❯ mkdir -p /tmp/{usb0,usb1}
❯ sudo cryptsetup open /dev/sda1 usb
Enter passphrase for /dev/sda1:
❯ sudo mount /dev/mapper/usb /tmp/usb0
❯ sudo mount /dev/sda2 /tmp/usb1
```

Les étapes sont les suivantes:
1. Création de deux dossiers destinés au montage des partitions.
2. Ouverture de la partition chiffrée en renseignant la *phrase de passe*.
3. Montage de la partition chiffrée à travers `dm_crypt` via `/dev/mapper/usb` dans le premier dossier.
4. Montage direct de la partition non chiffrée dans le second dossier.

À partir de là, il est possible de lire et écrire dans `/tmp/usb0` et `/tmp/usb1` qui correspondent aux partitions chiffrée et non chiffrée respectivement.

### Démontage

Avant de retirer la clé USB de la machine, il est nécessaire de démonter proprement les volumes et de fermer la partition chiffrée.

```
❯ sudo umount /tmp/usb1
❯ sudo umount /tmp/usb0
❯ sudo cryptsetup close usb
```

Les étapes sont les suivantes:
1. Démontage de la partition non chiffrée.
2. Démontage de la partition chiffrée.
3. Fermeture de la partition chiffrée.

## Note

* Il est possible de se passer de l'étape de création de partitions si l'on souhaite utiliser tout l'espace de la clé USB chiffré.
  Il suffit d'aller directement à l'étape de chiffrement et de ne pas préciser de numéro de partition : `sudo cryptsetup luksFormat /dev/sda`.

* Sources:
    * https://wiki.archlinux.org/index.php/dm-crypt/Device_encryption
    * https://gitlab.com/cryptsetup/cryptsetup/blob/master/docs/on-disk-format-luks2.pdf
