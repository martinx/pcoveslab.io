+++
title = "Blog"
description = "What I often need but always forget"

weight = 0

sort_by = "date"
paginate_by = 6

generate_feed = true
+++

Here is the place I ramble.
Always about technology but scarcely the same topic from one post to another.

I mostly write about programming and associated tools.
Whether it's about personal or professional challenges, I try to describe the issue, list possible solutions and dive into the chose one.

I also share some *tutos* for my fellow coworkers when the same question comes back to often.
Mostly in the [French side of the blog](/blog) though.

Enjoy!
