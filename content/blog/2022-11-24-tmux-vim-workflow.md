+++
title = "TMux et NVim workflow"
description = "Petite présentation de mon flux de travail pour les collègues"

[taxonomies]
sujets = ["IT", "vim", "neovim", "tmux", "alacritty"]

[extra]
thumbnail = "neomux.png"
+++

Il m'arrive très souvent au travail de faire de petites présentations.
Que ce soit pour *vendre* une nouvelle idée ou *fêter* la fin d'un projet, les occasions ne manquent pas.

Cette fois-ci, c'est une présentation à destination d'un collègue particulier.
Il travaille beaucoup en console comme moi mais ne prends pas le temps de configurer son environnement.
Je lui avais promis ce document il y a fort longtemps et je me suis dit que ce serait dommage de ne pas le partager au *reste du monde*.

[**Enjoy**](https://pcoves.gitlab.io/my-tmux-vim-workflow)

<!-- more -->
