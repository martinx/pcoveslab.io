+++
title = "Neovim + LibUV = <3"
description = "Description"

[taxonomies]
sujets = ["Neovim", "NVim", "LibUV"]

[extra]
thumbnail = "libuv.png"
+++

Récente découverte pour moi: [`neovim`]( https://neovim.org ) expose une partie de la [`libuv`]( https://libuv.org ).

Cette bibliothèque permet de gérer des évènements asynchrones.
Parmi eux sont envisageables la gestion de requêtes externes en TCP ou UDP ou bien l'appel de commandes lorsqu'un fichier est modifié sur le disque.
C'est cette seconde possibilité que je vais aborder dans cet article.

J'écris de plus en plus de choses en [`markdown`]( https://www.markdownguide.org/ ) avec [`pandoc`]( https://pandoc.org/ ) pour la conversion en `html` et/ou `pdf`.
J'ai récemment rédigé [un article]( en/blog/nvim-and-pandoc/ ) sur comment configurer `neovim` pour la gestion du `markdown` pour `pandoc` ou `revealjs`.
Ce que je présente ici va plus loin en automatisant plus encore les taches de générations en reposant sur la `libuv` et, ce faisant, sans bloquer l'interface de l'éditeur à chaque sauvegarde.

<!-- more -->

## Le besoin

Dans mon précédent article, je déclenchais un appel à `:make<CR>` à chaque sauvegarde du *buffer* sur disque.
En ayant préalablement redéfini `makeprg`, un document `html` était généré à chaque `:w<CR>`.

Ça fonctionne, pas de doute là-dessus.
Cependant, il y a une petite gêne: l'interface s'arrête une fraction de seconde, le temps que l'appel à `pandoc` soit terminé.

Dans ce cas de documents courts ou sur une machine puissante, c'est à peine visible.
Mais qu'en est-il des situations plus raisonnables en termes de machine hôte ou de contenu à rallonge?

## La solution

Surprise surprise... `libuv` à la rescouse!
Qui aurait pu le prévoir?

En deux parties: un code générique et du code spécifique appelant le premier.
Après tout, rares sont les développeurs qui aiment se répéter.

### `Watch.lua`

Comme d'habitude, je commence par le code à placer dans `~/.config/nvim/lua/watch.lua`:

```lua,linenos,hl_lines=3 5 12
local M = {}

local event = nil

function M.stop()
    if event and event:is_active() then
        event:stop()
        event = nil
    end
end

function M.watch(callback, path)
    path = path or vim.fn.expand("%")

    M.stop()

    event = vim.loop.new_fs_event()
    event:start(path, {}, function(err, filename, events)
        assert(not err, err)

        if events.change then
            callback(filename)
        end
    end)
end

return M
```

Il s'agit d'un petit module `lua` définissant un *event* local et deux fonctions publiques:

* La première, `stop` s'occupe d'arrêter la boucle de surveillance (l'*event*) si elle existe.
* La seconde, `watch` est en charge de débuter la surveillance d'un fichier sur le disque (par défaut le fichier relatif au *buffer* courant) et appeler une fonction *callback* à chaque modification détectée.

{% note() %}
La `libuv` est accessible via `vim.loop`.

Pour plus d'infos `:help uv<CR>`.
{% end %}

Ce bout de code ne contient aucune information précise quant au traitement à faire subir au fichier.
Ce n'est qu'une façon de partager la logique de surveillance des fichiers, celle du traitement doit être fournie par un code appelant.

### `Pandoc.lua`

Je reprends le fichier `~/.config/nvim/after/ftplugin/pandoc.lua` car cet article est la suite logique de celui cité en introduction.
Le raisonnement présenté ici est bien sûr portable à mon `revealjs.lua` ou autres `plantuml.lua` pour la génération de schémas.

Et, pour continuer comme avant, du code:

```lua,linenos,hl_lines=1 4-11 23 28
local watch = require("watch")

local function make(path)
    local handle = vim.loop.spawn("pandoc", {
        args = {
            path,
            "--to=html5",
            "--standalone",
            "--output", vim.fn.fnamemodify(path, ":r") .. ".html"
        }
    }, function(code, signal)
        assert(code, code)
        assert(signal, signal)
    end)

    handle:close()
end

local pandoc = vim.api.nvim_create_augroup("pandoc", { clear = true })

vim.api.nvim_create_autocmd({ "BufEnter" }, {
    group = pandoc,
    callback = function() watch.watch(make) end
})

vim.api.nvim_create_autocmd({ "BufLeave" }, {
    group = pandoc,
    callback = function() watch.stop() end
})
```

Je détaille les quatre parties surlignées ci-dessous:

* J'importe `watch.lua`, le module défini dans la section précédente.
  J'ai donc accès aux fonctions `watch` et `stop`.
* Je définis dans la fonction `make` ce qu'il faut faire du fichier lorsqu'un changement est détecté.
  Ici, je me contente de générer une version `html` du contenu dans le même dossier que la source en reprenant le reste du nom di fichier.
* Je commence à surveiller le fichier quand je rentre dans un *buffer* dont le `filetype` est `pandoc`.
* J'arrête la surveillance dans je sors dudit *buffer*.

## Conclusion

L'intégration de la `libuv` dans `neovim` permet de lancer des calculs lourds en parallèle de l'édition du code.
Ici il ne s'agit que de convertir du `markdown` en `html` mais des cas d'usages plus avancés sont triviaux à mettre en place.
La documentation, truffée d'exemple, est une excellente source d'inspiration.

En termes d'expérience utilisateur, je suis content de ne plus avoir ce petit *lag* à chaque sauvegarde.
Il ne me manque plus qu'un moyen de signaler à mon navigateur que la page web a été renouvelée et qu'il faut recharger l'onglet.
Peut-être pour un prochain article?
