+++
title = "Tupper `git` : Worktree"
description = "Hier soir j'ai présenté les *worktrees* de `git` à l'occasion d'un TupperGit"

[taxonomies]
sujets = ["TupperGit", "Présentation", "Slides"]
+++

Hier soir j'ai donné une courte présentation à propos des *worktrees* de `git`.

Cela a eu lieu dans le cadre des *TupperGit*.
Il s'agit de tours de tables informels autour d'un sujet précis, ici `git`.

Les slides sont accessibles [ici](https://pcoves.gitlab.io/tupper-git_worktree/).

<!-- more -->

Tout comme lors de ma [précédente présentation](/blog/human-talks-docker-multistage-distroless/) lors des [*Human Talks*](https://humantalks.com/) il y a un mois et des brouettes, ce fut une super expérience.

Parler d'un sujet technique à un public allant de débutant à confirmé est l'assurance d'échanges de qualité et de découvertes formidables.

Au passage, en recherchrant deux trois choses pour cette présentation, mon moter de recherche favori m'a conseillé [cet article](/blog/git-worktree/).
C'est marrant de se voir recommandé à soi même.
Mais bon, n'est-ce pas le but même de ce site : un pense-bête pour le moi du futur !

Un grand merci à [`Chouhartem`](https://blog.epheme.re/fr/author/fabrice.html), un habitué des réunions *TupperVim* et *TupperGit* pour ses corrections et conseils!
