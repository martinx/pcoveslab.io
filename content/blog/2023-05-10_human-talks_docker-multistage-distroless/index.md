+++
title = "Human talks: Docker + multistage + distroless"
description = "Hier soir j'ai présenté les fonctionnalités de `multistage` et `distroless` de docker lors d'une intervention aux Human Talks de Grenoble"

[taxonomies]
sujets = ["Human talks", "docker", "multistage", "distroless"]
+++

Hier soir j'ai parlé lors des [*Human Talks*](https://humantalks.com/) de Grenoble.

J'ai eu dix minutes pour introduire deux fonctionnalités de `docker`: `multistage` et `distroless` à un public intéressé et impliqué.

Les slides sont accessibles [ici](https://pcoves.gitlab.io/human-talks_docker).

<!-- more -->

C'était la première fois que je prenais publiquement la parole en dehors de mon travail.
Et bien aucun regret, c'était extrêmement enrichissant tant pour moi que pour mon auditoire si j'en crois les retours.
