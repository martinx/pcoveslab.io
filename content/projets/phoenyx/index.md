+++
title = "Phœnyx"
description = "Un thème pour Zola"

weight = 2

[extra.theme.dark]
thumbnail = "/projets/phoenyx/thumbnail-light.svg"

[extra.theme.light]
thumbnail = "/projets/phoenyx/thumbnail-dark.svg"
+++

Ce site est généré avec `Zola`.
Il existe de nombreux autres générateurs de sites statiques mais `Zola`, en plus d'être écrit en `Rust` :crab:, a le bon goût d'allier puissance et simplicité.

Il est possible de créer des *templates* génériques pour ensuite les utiliser tels quels ou bien hériter de certains traits et altérer les détails de contenu ou de présentation.

`Phœnyx` est un *template* profitant de l'internationalisation, proposant plusieurs thèmes de couleurs et à la fois facile à utiliser et à étendre.

<!-- more -->

{% quote(author = "[Site web de `Zola`](https://www.getzola.org/)") %}
Your one-stop static site engine

Forget dependencies. Everything you need in one binary.
{% end %}

## Le code

[Hébergé sur `Gitlab`](https://gitlab.com/pcoves/phoenyx).

Ce dépôt contient non seulement le code source du *template* mais aussi le *pipeline* de `CI/CD` permettant de générer automatiquement des *`Gitlab` pages* avec.
Le résultat du *template* "nu" est visible [ici](https://pcoves.gitlab.io/phoenyx/).

## L'architecture

`Phœnyx` est conçu pour être à la fois rapide à utiliser sans modification et facile à dériver.

### Thèmes

Un exemple de cette flexibilité est l'ajout de thèmes de couleurs: ce site descend de `phœnyx` et dispose de trois ensemble de couleurs accessibles via les :sparkles: en hauts et bas de pages si `Javascript` est actif (au moment de l'écriture de ces lignes) alors que `phœnyx` n'en expose que deux par défaut.

### `Javascript`

En termes d'accessibilité, `phœnyx` n'utilise presque pas de `Javascript`.
Et chaque fonctionnalité qui en dépend est cachée si `Javascript` est inutilisable afin de ne pas encombrer le rendu final.
Typiquement, le choix des thèmes disparaît si `Javascript` n'est pas au rendez-vous et un thème clair est activé par défaut.
La méthode utilisée est détaillée [ici](/blog/hide_html_when_noscript).

### Héritage

La méthode pour utiliser [`phœnyx`](https://pcoves.gitlab.io/phoenyx/) sera détaillée dans ses pages sous peu.
En attendant, il "suffit" de regarder le code de [ce site](https://gitlab.com/pcoves/pcoves.gitlab.io) pour constater qu'il faut très peu de travail pour obtenir un résultat rapidement.

## Conclusion

J'ai longtemps pensé à faire un thème générique pour `Zola` mais sans jamais vraiment obtenir un résultat satisfaisant à mes yeux.
Avec `phœnyx`, je sais que les sites générés sont sobres mais efficaces.

La seule chose qui me chagrine un peu est le fait de devoir copier les traductions du *template* parent au site enfant.
Mais il s'agit d'une limitation connue de `Zola` et en aucun cas un problème propre à `phœnyx`.

Si jamais ce *template* t'intéresse, n'hésite pas à me faire signe en cas de problème ou difficulté d'usage dans une *issue* `Gitlab`.
Je me ferai un plaisir de répondre à tes questions ou fixer/améliorer le code pour tes cas d'usages!
