+++
title = "Home"
description = "There's no place like nest"

sort_by = "weight"

[extra.hero]
title = "Autruche"
description = """
Autruches (French for Ostriches) are neat!

I mean, just look at them !
Their brain is smaller than their eyeballs...
And yet, here they are, ~~walking~~ running [dinosaurs](https://en.wikipedia.org/wiki/Ornithomimosauria) amongst us.

**Fun fact** : they don't really burry their head in the sand when scared.
They're in fact tending to their eggs, hence, the mith.
"""
+++
